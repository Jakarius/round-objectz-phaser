
var balls = [];
var ghostBall;

var maxBalls = 1;
var maxBallsText;

var ballTimer;
var mouseDown;
var mouseUp;
var ballStartPos;

var ballsLaunched = 0;

var BALLSPEED = 800;

var blockSize = 64;
var paddingSize = 4;
var SHIFT_AMOUNT = blockSize + paddingSize;

var ballHitSideAlready = false;

var ballTrajectory;

var ballCollisionGroup;
var boxCollisionGroup;

var firstBallDown = true;

var standardSpeedVector;
var fastSpeedVector;

var boxes = [];

var roundScore = 1;
var roundScoreText;

var topBanner;

var UIGroup;
var entityGroup;

var roundInProgress = false;

var playState = {

	create: function() {

		BALL_START_Y_OFFSET = 60;
		BALL_BOTTOM_TRIGGER = GAME_HEIGHT - 12 - 15;

		ballStartPos = new Phaser.Point(game.world.centerX, GAME_HEIGHT - BALL_START_Y_OFFSET);

		game.physics.startSystem(Phaser.Physics.P2JS);
		game.physics.p2.setImpactEvents(true);
		game.physics.p2.restitution = 1.0;
		game.physics.p2.applyDamping = false;

		ballCollisionGroup = game.physics.p2.createCollisionGroup();
		boxCollisionGroup = game.physics.p2.createCollisionGroup();
		UICollisionGroup = game.physics.p2.createCollisionGroup();
		leftRightCollisionGroup = game.physics.p2.createCollisionGroup();
		game.physics.p2.updateBoundsCollisionGroup();

		game.input.mouse.capture = true;

		// game.input.activePointer.leftButton.onDown.add(drawAimingLine, this);
		game.input.activePointer.leftButton.onUp.add(shootBalls, this);

		UIGroup = game.add.group();
		entityGroup = game.add.group();
		mousePosText = game.add.text(5,5, "", {fill: '#ffffff'});

		ghostBall = game.add.sprite(ballStartPos.x, ballStartPos.y, "white_ball");

		leftCollider = game.add.sprite(0, GAME_HEIGHT / 2);
		leftCollider.height = GAME_HEIGHT;
		leftCollider.width = 15;
		game.physics.p2.enable(leftCollider);
		leftCollider.body.static = true;
		leftCollider.body.setCollisionGroup(leftRightCollisionGroup);
		leftCollider.body.collides([ballCollisionGroup]);

		rightCollider = game.add.sprite(GAME_WIDTH, GAME_HEIGHT / 2);
		rightCollider.height = GAME_HEIGHT;
		rightCollider.width = 15;
		game.physics.p2.enable(rightCollider);
		rightCollider.body.static = true;
		rightCollider.body.setCollisionGroup(leftRightCollisionGroup);
		rightCollider.body.collides([ballCollisionGroup]);


		// box.body.setZeroDamping();
		// box.body.moves = false;
		// box.body.immovable = true;
		// box.body.friction = new Phaser.Point(0,0);
		// box.body.bounce = new Phaser.Point(1,1);


		bitmap = game.add.bitmapData(game.width, game.height);
	    bitmap.context.fillStyle = 'rgb(255, 255, 255)';
	    bitmap.context.strokeStyle = 'rgb(255, 255, 255)';
	    game.add.image(0, 0, bitmap);


	    topBanner = game.make.sprite(0, 0, "top_banner");
	    topBanner.x = topBanner.width / 2;
	    topBanner.y = topBanner.height / 2;
	    game.physics.p2.enable(topBanner);
	    topBanner.body.static = true;
	    topBanner.body.setCollisionGroup(UICollisionGroup);
	    topBanner.body.collides([ballCollisionGroup]);

	    bottomBanner = game.add.sprite(0, 0, "top_banner");
	    bottomBanner.x = bottomBanner.width / 2;
	    bottomBanner.y = GAME_HEIGHT - 12;
	    game.physics.p2.enable(bottomBanner);
	    bottomBanner.body.static = true;
	    bottomBanner.body.setCollisionGroup(UICollisionGroup);
	    // bottomBanner.body.collides([ballCollisionGroup]);

	    UIGroup.add(topBanner);
	    UIGroup.add(bottomBanner);

	    createBoxLine();

	    game.add.existing(topBanner);
	    roundScoreText = game.add.text(game.world.centerX, 10, "0", { fill: "#ffffff", font: "23px Righteous"});

	    maxBallsText = game.add.text(0, 0, getMaxBallsString(), { fill: "#ffffff", font: "16px Righteous" } );
	    updateMaxBallsTextRoundEnd(ballStartPos.x, ballStartPos.y);

	    game.physics.p2.setPostBroadphaseCallback(checkBallPickup, this);


	    fastForwardIcon = game.add.sprite(0, 9, "fast_forward");
	    fastForwardIcon.x = game.world.width - fastForwardIcon.width - 10;
	    fastForwardIcon.inputEnabled = true;
	    fastForwardIcon.events.onInputDown.add(enterFastMode, this);

	    // var ballMaterial = game.physics.p2.createMaterial('ballMaterial', ball.body);
     //    var worldMaterial = game.physics.p2.createMaterial('worldMaterial');

     //    box.body.setMaterial("worldMaterial");

     //    game.physics.p2.setWorldMaterial(worldMaterial, true, true, true, true);

	    // var contactMaterial = game.physics.p2.createContactMaterial(ballMaterial, worldMaterial);
     //    contactMaterial.friction = 2;     // Friction to use in the contact of these two materials.
     //    contactMaterial.restitution = 1;  // Restitution (i.e. how bouncy it is!) to use in the contact of these two materials.
     //    contactMaterial.stiffness = 1e7;    // Stiffness of the resulting ContactEquation that this ContactMaterial generate.
     //    contactMaterial.relaxation = 1;


     //    ballMaterial.friction = 2;
     //    ballMaterial.restitution = 1.0;

     	// ballVelocityVec = new Phaser.Point();
     	// ballVelocityVec.set(0,0);
	},

	update: function() {

		// if (Math.abs(ballVelocityVec.getMagnitudeSq() - BALLSPEED * BALLSPEED) > 0.01) {
		// 	console.log("BEEP");
		// 	ballVelocityVec.setMagnitude(BALLSPEED);
		// 	ball.body.velocity.x = ballVelocityVec.x;
		//     ball.body.velocity.y = ballVelocityVec.y;
		// }

		for (var i = 0; i < balls.length; i++) {
			if (balls[i].y >= BALL_BOTTOM_TRIGGER) {
				if (firstBallDown) {
					handleFirstBallDown(balls[i]);
				}
				balls[i].destroy();
				balls.splice(i, 1);
				if (balls.length === 0) {
					endRound();
				}
			}
		}

		if (game.input.activePointer.leftButton.isDown) {
			drawAimingLine();
		}

		// game.physics.arcade.collide(ball, box, hitBox);

	},

	render: function() {
		// game.debug.body(ball);
		// game.debug.body(box);
		// mousePosText.text = "x=" + game.input.activePointer.position.x + "  y="+game.input.activePointer.position.y;
	}

}

function handleFirstBallDown(ball) {
	var newBallPos = ball.position;
	newBallPos.y = GAME_HEIGHT - 55;
	ghostBall.x = newBallPos.x;
	ghostBall.y = newBallPos.y;
	ghostBall.alpha = 1;
	firstBallDown = false;
	// updateMaxBallsTextRoundEnd(ghostBall.x, ghostBall.y);
	// updateMaxBallsTextMidRound();
}

function getMaxBallsString() {
	return "x" + maxBalls;
}

function checkBallHittingSides(ballBody) {
	if (ballBody.lastHitSide) {
		console.log(ballBody.velocity.y);
		ballBody.velocity.y = ballBody.velocity.y + 2;
		console.log(ballBody.velocity.y);
	}
	ballBody.lastHitSide = true;
}

function checkBallPickup(body1, body2) {
	if (body1.sprite != null && body1.sprite.key === "white_ball" && body2.sprite != null && body2.sprite.key === "ball_pickup" ) {
		ballHitsBox(body1, body2);
		return false;
	} else if (body2.sprite != null && body2.sprite.key === "white_ball" && body1.sprite != null && body1.sprite.key === "ball_pickup") {
		ballHitsBox(body2, body1);
		return false;
	}

	return true;
}

function shootBalls() {

	if (ballsLaunched > 0) {
		return;
	}

	standardSpeedVector = getBallVector();
	if (isNaN(standardSpeedVector.y) ||  standardSpeedVector.y >= 0) {
		return;
	}

	// maxBallsText.visible = false;

	game.world.bringToTop(UIGroup);

	balls = [];
	firstBallDown = true;
	ghostBall.alpha = 0.5;
	bitmap.context.clearRect(0, 0, game.width, game.height);
	bitmap.dirty = true;

	// console.log(up.x + "-" + down.x + "=" + deltaX + "    " + up.y + "-" + down.y + "=" + deltaY);


	// ballVelocityVec.set(ball.body.velocity.x, ball.body.velocity.y);
	ballTimer = game.time.create();
	ballShootTimerEvent = ballTimer.loop(Phaser.Timer.SECOND * 0.15, shootBall, this, standardSpeedVector);
	ballTimer.start();

	roundInProgress = true;
}

function incrementBallsLaunched() {
	ballsLaunched++;
	if (ballsLaunched >= maxBalls) {
		ballTimer.stop();
		ballTimer.destroy();
	}
}

function shootBall(vector) {
	incrementBallsLaunched();
	var ball = createBall();
	ball.body.velocity.x = vector.x;
	ball.body.velocity.y = vector.y;
	balls.push(ball);
	updateMaxBallsTextMidRound();
}

function getBallVector() {
	mouseDown = game.input.activePointer.positionDown;
	mouseUp = game.input.activePointer.position;
	// console.log(mouseDown);
	// console.log(mouseUp);

	var deltaX = mouseUp.x - mouseDown.x;
	var deltaY = mouseUp.y - mouseDown.y;
	var mag = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));

	var normX = deltaX / mag;
	var normY = deltaY / mag;

	var scaledX = normX * -BALLSPEED;
	var scaledY = normY * -BALLSPEED;

	return {x: scaledX, y: scaledY};
}

function createBall() {
	var ball = game.add.sprite(ballStartPos.x, ballStartPos.y, "white_ball");
	ball.anchor.setTo(0.5,0.5);
	game.physics.p2.enable(ball);
	ball.body.setCircle(10);
	ball.body.collideWorldBounds = true;
	ball.body.fixedRotation = true;

	// Set balls to be in a group that doesn't collide with other balls.
	ball.body.setCollisionGroup(ballCollisionGroup);
    ball.body.collides([boxCollisionGroup, UICollisionGroup, leftRightCollisionGroup]);
    ball.body.createGroupCallback(boxCollisionGroup, ballHitsBox, this);
    ball.body.createGroupCallback(leftRightCollisionGroup, checkBallHittingSides, this);
    ball.body.lastHitSide = false;

    entityGroup.add(ball);

	return ball;
}

function ballHitsBox(ballBody, boxBody) {
	if (boxBody.sprite != null && boxBody.sprite.isBox) {
		ballBody.lastHitSide = false;
		boxBody.sprite.health -= 1;
		updateBoxHealthText(boxBody.sprite);
		if (boxBody.sprite.health <= 0) {
			boxBody.removeNextStep = true;
			boxBody.sprite.destroyTween.start();
			for (var i = 0; i < boxes.length; i++) {
				if (boxBody.sprite === boxes[i]) {
					// console.log("destroying box  " + i);
					// boxes[i].destroy();
					boxes.splice(i, 1);
				}
			}
		}
	} else {
		maxBalls += 1;
		boxBody.removeNextStep = true;
		for (var i = 0; i < boxes.length; i++) {
			if (boxBody.sprite === boxes[i]) {
				console.log("destroying box  " + i);
				boxes[i].destroy();
				boxes.splice(i, 1);
			}
		}
	}
}

function updateBoxHealthText(boxSprite) {
	boxSprite.healthText.text = boxSprite.health;
}

function drawAimingLine() {
	var mouseDownCurrent = game.input.activePointer.positionDown;
	var currentMouse = game.input.activePointer.position;
	ballTrajectory = new Phaser.Line(mouseDownCurrent.x, mouseDownCurrent.y, currentMouse.x, currentMouse.y);
	bitmap.context.clearRect(0, 0, game.width, game.height);
	bitmap.context.beginPath();
    bitmap.context.moveTo(mouseDownCurrent.x, mouseDownCurrent.y);
    bitmap.context.lineTo(currentMouse.x, currentMouse.y);
    bitmap.context.stroke();
    bitmap.context.lineWidth = "4";
    bitmap.context.setLineDash([16,16]);
    bitmap.dirty = true;
}

function enterFastMode() {

	if (!roundInProgress) {
		return;
	}

	// Speed up existing and future balls.
	standardSpeedVector.x = standardSpeedVector.x * 1.5;
	standardSpeedVector.y = standardSpeedVector.y * 1.5;
	for (var i = 0; i < balls.length; i++) {
		balls[i].body.velocity.x = balls[i].body.velocity.x * 1.5;
		balls[i].body.velocity.y = balls[i].body.velocity.y * 1.5;
	}

	// Shoot remaining balls on a new timer at a faster rate.
	if (maxBalls - ballsLaunched > 0) {
		ballTimer.stop();
		ballTimer.destroy();
		ballTimer = game.time.create();
		ballShootTimerEvent = ballTimer.loop(Phaser.Timer.SECOND * 0.08, shootBall, this, standardSpeedVector);
		ballTimer.start();
	}
}

function createBox(x, isBox) {
	var xPos = paddingSize + (x * (blockSize + paddingSize));
	var spriteKey = "";
	if (isBox) {
		spriteKey = "box";
	} else {
		spriteKey = "ball_pickup";
	}

	box = game.add.sprite(xPos, paddingSize + topBanner.height, spriteKey);

	game.physics.p2.enable(box);
	box.anchor.setTo(0,0);
	box.body.clearShapes();

	if (isBox) {
		box.body.addRectangle(blockSize, blockSize, blockSize / 2, blockSize / 2);
	} else {
		box.body.setCircle(18, blockSize/2, blockSize/2);
	}

	box.body.updateCollisionMask();
	box.body.static = true;

	if (isBox) {
		box.body.setCollisionGroup(boxCollisionGroup);
		box.body.collides(ballCollisionGroup);
		box.isBox = true;
		box.health = Math.floor((Math.random() * (roundScore - (roundScore / 2) + (roundScore / 2)) + 1));
		box.healthText = box.addChild(game.make.text(blockSize / 2, blockSize / 1.75, box.health, { fill: "#ffffff", font: "26px Righteous"}));
		box.healthText.anchor.setTo(0.5,0.5);
		box.tint = getBoxTint(box.health);
	} else {
		box.isBox = false;
	}

	box.tween = game.add.tween(box.body).from( { y: box.y }, 200, Phaser.Easing.Exponential.Out, false);
	box.destroyTween = game.add.tween(box.body).to({ x: Math.floor(Math.random() * GAME_WIDTH), y: GAME_HEIGHT + 64}, 500, Phaser.Easing.Back.InOut, false);
	box.destroyTween.onComplete.add( function(sprite) { sprite.destroy(); });

	entityGroup.add(box);

	return box;
}

function getBoxTint(health) {

	var i = (health * 255 / 100);
    var r = Math.round(Math.sin(0.024 * i + 0) * 127 + 128);
    var g = Math.round(Math.sin(0.024 * i + 2) * 127 + 128);
    var b = Math.round(Math.sin(0.024 * i + 4) * 127 + 128);

    var rHex = rgbToHex(r);
    var gHex = rgbToHex(g);
    var bHex = rgbToHex(b);
    var hex = "0x" + rHex + gHex + bHex;
    return parseInt(hex);
}

function rgbToHex (rgb) {
	var hex = Number(rgb).toString(16);
	if (hex.length < 2) {
		hex = "0" + hex;
	}
	return hex;
}

function createBoxLine() {
	var rand = 0;
	for (var i = 0; i < GAME_WIDTH / blockSize - 1; i++) {
		rand = Math.random();
		if (rand < 0.5) {
			var box = createBox(i, true);
			boxes.push(box);
		} else if (rand > 0.9) {
			boxes.push(createBox(i, false));
		}
	}
}

function shiftBoxesDown() {
	console.log("shifting");
	for (var i = 0; i < boxes.length; i++) {
		boxes[i].body.y += SHIFT_AMOUNT;
		boxes[i].tween.start();
		if (boxes[i].y >= GAME_HEIGHT - blockSize - 10) {
			gameOver();
		}
	}
}

function endRound() {

	roundInProgress = false;

	roundScore++;
	roundScoreText.text = roundScore;
	ballStartPos.x = ghostBall.x;
	ballStartPos.y = ghostBall.y;
	shiftBoxesDown();
	createBoxLine();
	ballsLaunched = 0;
	updateMaxBallsTextRoundEnd(ballStartPos.x, ballStartPos.y);
}

function updateMaxBallsTextRoundEnd(x,y) {
	maxBallsText.x = x + 18;
	maxBallsText.y = y - 18;
	maxBallsText.text = "x" + maxBalls;
	maxBallsText.visible = true;
}

function updateMaxBallsTextMidRound() {
	var ballsRemaining = maxBalls - ballsLaunched;
	maxBallsText.text = "x" + ballsRemaining;
}

function gameOver() {
	var gameOverText = game.add.text(game.world.centerX, game.world.centerY, "GAME OVER", { fill: "#ffffff"} );
	gameOverText.anchor.setTo(0.5,0.5);
}

// function resetBall(ball) {
// 	ball.body.x = ballStartPos.x;
// 	ball.body.y = ballStartPos.y;
// 	ball.body.velocity.x = 0;
// 	ball.body.velocity.y = 0;
// }
