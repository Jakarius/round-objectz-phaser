var loadState = {

    preload: function () {
        assetsPath = "assets/";

        //UI
        game.load.image('top_banner', assetsPath + 'top_banner.png');
        game.load.image('fast_forward', assetsPath + 'fast_forward.png');

        //Balls
        game.load.image('white_ball', assetsPath + 'white_ball.png');


        //Boxes
        game.load.image('box', assetsPath + 'box.png');
        game.load.image('ball_pickup', assetsPath + 'ball_pickup.png');

        game.load.start();
    },

	create: function() {
		game.state.start('play');
	}
}